import React, { useState } from "react";

export default function StateDemoFC() {
  const [counter, setCounter] = useState(10);
  return <div>Counter is : {counter}</div>;
}
