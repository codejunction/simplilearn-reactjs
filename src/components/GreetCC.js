import React, { Component } from "react";

class GreetCC extends Component {
  render() {
    return <h1>This is a class component</h1>;
  }
}

export default GreetCC;
