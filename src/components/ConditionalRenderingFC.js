import React from "react";

function GreetUser(props) {
  return <div>Welcome {props.uname}</div>;
}

function GreetGuest() {
  return <div>Welcome Guest</div>;
}

export default function ConditionalRenderingFC(props) {
  const isLoggedIn = props.isLoggedIn;

  if (isLoggedIn) {
    // return <GreetUser {...props} />;
    return <GreetUser uname={props.uname} />;
  } else {
    return <GreetGuest />;
  }
}
