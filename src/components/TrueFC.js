import React from "react";

export default function TrueFC(props) {
  return <div>Hello, {props.name}!</div>;
}
