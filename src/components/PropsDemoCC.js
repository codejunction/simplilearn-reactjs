import React, { Component } from "react";

export default class PropsDemoCC extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // destructuring props
    const { uname, pword } = this.props;
    return (
      <div>
        {/* Hello {this.props.uname}, you stay at {this.props.city} and have phone
        number {this.props.phoneNumber}
        <br />
        {this.props.children} */}
        Hello {uname}
        <br />
        Password is : {pword}
        <br />
        {this.props.children}
      </div>
    );
  }
}

PropsDemoCC.defaultProps = {
  uname: "Guest",
  pword: "Not Specified",
  city: "New York",
  phoneNumber: "123-456-7890",
};
