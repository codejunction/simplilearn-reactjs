import React, { Component } from "react";
import TrueFC from "./TrueFC";
import FalseFC from "./FalseFC";

export default class ConditionalRendering extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUserValid: true,
      uname: "John",
    };
  }
  render() {
    // Method 1 using JSX in if
    // if (this.state.isUserValid === true) {
    //   return <div>Hello, {this.state.uname}</div>;
    // } else {
    //   return <div>Invalid Username or Password</div>;
    // }

    // Method 2 using response
    // let response;
    // if (this.state.isUserValid) {
    //   response = `Welcome ${this.state.uname}!`;
    // } else {
    //   response = `Invalid Username or Password`;
    // }
    // return <div>{response}</div>;

    // Method 3 using ternary operator
    // return this.state.isUserValid ? (
    //   <div> Welcome, {this.state.uname} ! </div>
    // ) : (
    //   <div>Invalid Username or Password</div>
    // );

    // Method 4 using short-circuit operator (something or nothing)
    // return this.state.isUserValid && <div>Welcome {this.state.uname}</div>;

    return this.state.isUserValid ? (
      <TrueFC name={this.state.uname} />
    ) : (
      <FalseFC />
    );
  }
}
