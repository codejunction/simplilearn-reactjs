import React, { Component } from "react";

export default class EventHandling extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: "John",
    };
  }
  //   clickHandler = () => {
  //     console.log("Button is clicked");
  //     // this.state.uname = "Smith";
  //     this.setState({ uname: "Smith" });
  //   };
  clickHandler() {
    console.log("Button is clicked");
    // this.state.uname = "Smith";
    this.setState({ uname: "Smith" });
  }
  render() {
    return (
      <div>
        Hello {this.state.uname}
        <br />
        <button onClick={this.clickHandler}>Click</button>
      </div>
    );
  }
}
