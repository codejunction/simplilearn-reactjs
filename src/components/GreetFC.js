import React from "react";

//default export
// function GreetFC() {
//   return (
//     <div>
//       <h1>Hello World</h1>
//       <p>This is paragraph from GreetFC</p>{" "}
//     </div>
//   );
// }

// export default GreetFC;

//named export
export const GreetFC = () => {
  return (
    <div>
      <h1>Hello World</h1>
      <p>This is paragraph from GreetFC</p>{" "}
    </div>
  );
};
