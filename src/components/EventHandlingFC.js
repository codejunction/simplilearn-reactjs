import React from "react";

export default function EventHandlingFC() {
  const [counter, setCounter] = React.useState(0);
  return (
    <div>
      Counter is : {counter}
      <br />
      <button onClick={() => setCounter(counter + 1)}>Click</button>
    </div>
  );
}
