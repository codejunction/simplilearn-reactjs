import React from "react";

export default function PropsDemoFC(props) {
  return (
    <div>
      Hello, {props.uname}, you stay at {props.city}
      <br />
      {props.children}
    </div>
  );
}

PropsDemoFC.defaultProps = {
  uname: "Guest",
  city: "Nowhere",
};
