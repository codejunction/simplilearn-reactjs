// import logo from "./logo.svg";
import "./App.css";
import { GreetFC } from "./components/GreetFC";
import GreetCC from "./components/GreetCC";
import PropsDemoFC from "./components/PropsDemoFC";
import PropsDemoCC from "./components/PropsDemoCC";
import StateDemoCC from "./components/StateDemoCC";
import StateDemoFC from "./components/StateDemoFC";
import EventHandling from "./components/EventHandling";
import EventBinding1 from "./components/EventBinding1";
import EventBinding2 from "./components/EventBinding2";
import EventBinding3 from "./components/EventBinding3";
import EventBinding4 from "./components/EventBinding4";
import EventHandlingFC from "./components/EventHandlingFC";
import ConditionalRendering from "./components/ConditionalRendering";
import ConditionalRenderingFC from "./components/ConditionalRenderingFC";

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <h1>Welcome to React JS</h1>
      <p>This is message from react app</p>
      {/* <GreetFC />
      <GreetCC />
      <PropsDemoFC uname="Abhishek" city="Hyderabad">
        This is PropsDemoFC with props
      </PropsDemoFC>
      <PropsDemoFC>This is PropsDemoFC with no props</PropsDemoFC> */}
      {/* <hr/>
      <PropsDemoFC uname="John" city="Texas">
        This is some other text.
      </PropsDemoFC>
      <hr/>
      <PropsDemoFC uname="Karen" city="Seattle">
        This is some different text.
      </PropsDemoFC> */}
      {/* <PropsDemoCC
        uname="Karen"
        city="Seattle"
        phoneNumber="1324768"
        pword="166445"
      >
        This is PropsDemoCC
      </PropsDemoCC>
      <PropsDemoCC uname="Julia" city="Texas" phoneNumber="1324768">
        This is PropsDemoCC
      </PropsDemoCC>
      <PropsDemoCC uname="Abhishek" city="Houston" phoneNumber="Abhishek123">
        This is PropsDemoCC
      </PropsDemoCC>
      <StateDemoCC />
      <StateDemoFC /> */}
      {/* <EventHandling /> */}
      <EventBinding1 />
      <EventBinding2 />
      <EventBinding3 />
      <EventBinding4 />
      <EventHandlingFC />
      <ConditionalRendering />
      <ConditionalRenderingFC isLoggedIn={true} uname="Abhishek" />
    </div>
  );
}

export default App;
